//
//  AppDelegate.swift
//  swift-test-01
//
//  Created by Damian Skoneczny on 19.04.2017.
//  Copyright © 2017 Damian Skoneczny. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

