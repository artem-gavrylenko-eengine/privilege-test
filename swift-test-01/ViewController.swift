//
//  ViewController.swift
//  swift-test-01
//
//  Created by Damian Skoneczny on 19.04.2017.
//  Copyright © 2017 Damian Skoneczny. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    
    func wasLaunched(wasLaunched : Bool) {
        print(wasLaunched)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let openVPN = OpenVPN.init(pathToOVPNExecutable: "./")
        openVPN.start(completion: wasLaunched)
        
        /*let openVPN = OpenVPN.init(pathToOVPNExecutable: "./")
         let privil = openVPN.blessHelper(label: "Label")
        print(privil)
        if  privil {
            openVPN.start(completion: wasLaunched)
        }*/
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

